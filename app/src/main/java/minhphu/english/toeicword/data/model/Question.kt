package minhphu.english.toeicword.data.model

import java.util.*

class Question(val question: String, val correctAnswer: String, val correctAnswerDecrypt: String) {

    class AnswerSet(val answer: String, val answerDecrypt: String)

    val answerSet = ArrayList<AnswerSet>()

    var en: String? = null

    fun addAnswerSet(answer: String, answerDecrypt: String) {
        this.answerSet.add(AnswerSet(answer, answerDecrypt))
    }
}
