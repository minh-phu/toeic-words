package minhphu.english.toeicword.data.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import minhphu.english.toeicword.data.database.entities.Lesson
import minhphu.english.toeicword.data.database.repository.LessonRepository

class LessonViewModel(lessonRepository: LessonRepository) : ViewModel() {

    var mListLesson: LiveData<List<Lesson>> = lessonRepository.getListLesson()

}